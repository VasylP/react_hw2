import { Component } from "react";
import CardList from "../components/CardList";
import Header from "../components/Header";

export default class Home extends Component {
    constructor(props){
        super(props);
        this.state = {
            favorites: [],
            basket: [],
            products: null,
        }
    }
    componentDidMount = () => {
        fetch("./goods.json")
        .then((res)=>res.json())
        .then((res) => {
            this.setState({products: res});
        });
            let favProd = [];
            let basketProd = [];
            for (let key in localStorage){
                if(key.slice(0,3) === "fav"){
                    favProd.push(JSON.parse(localStorage.getItem(key)));
                } else if (key.slice(0,6) === "basket") {
                    basketProd.push(localStorage.getItem(key))
                }
            }
            return this.setState({favorites: Array.from(favProd), basket:  Array.from(basketProd)});
    }
    addFav = (e) => {
        const target = e.target.getAttribute('data-id');
        const prodArr = this.state.products;
        const favArr = this.state.favorites;
        prodArr.forEach(element => {
            if(target === element.barcode){
                localStorage.setItem(`fav ${element.name}`, JSON.stringify(element));
                favArr.push(element);
            }
        })
        return this.setState({favorites: favArr});
    }
    delFav = (e) => {
        const target = e.target.getAttribute('data-id');
        let newFavArr = [];
        const favArr = this.state.favorites;
        favArr.forEach(el => {
            if(el.barcode === target){
                localStorage.removeItem(`fav ${el.name}`);
            } else {
                newFavArr.push(el);
            }
            
        })
        return this.setState({favorites: newFavArr});
    }
    addBasket = (e) => {
        const target = e.target.getAttribute('data-id');
        const prodArr = this.state.products;
        const basketArr = this.state.basket;
        prodArr.forEach(element => {
            if(target === element.barcode){
                localStorage.setItem(`basket ${element.name}`, JSON.stringify(element));
                basketArr.push(element);
            }
        })
        return this.setState({basket: basketArr});
    }
    render(){
        if (!this.state.products) {
            return (
                <div>No information</div>
            )
        }
        return(
            <>
            <Header
            favCounter={this.state.favorites.length}
            basketCounter={this.state.basket.length}
            />
            <CardList
            products={this.state.products}
            favorites={this.state.favorites}
            addToFav={this.addFav}
            delFav={this.delFav}
            addToBasket={this.addBasket}
            />
            </>
        )
    }
}