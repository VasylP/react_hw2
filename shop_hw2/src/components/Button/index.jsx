import { Component } from "react";
import  PropTypes  from "prop-types";
import './button.scss';

export default class Button extends Component {
    constructor(props) {
        super(props);
    }
    render (){
        const { text, click, buttonId } = this.props;
        return (
            <>
            <button className="btn" onClick={click} data-id={buttonId}>{text}</button>
            </>
        )
    }
}

Button.propTypes = {
    text: PropTypes.string,
    click: PropTypes.func,
    buttonId: PropTypes.string,
};

Button.defaultProps = {
    text: "Button",
    click: () => {
        console.log("click");
    },
    buttonId: "product code",
};