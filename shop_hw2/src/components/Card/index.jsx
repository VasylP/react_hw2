import { Component } from "react";
import  PropTypes  from "prop-types";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar as favIcon } from "@fortawesome/free-solid-svg-icons";
import { faStar as noFav } from '@fortawesome/free-regular-svg-icons';
import Button from '../Button'
import "./card.scss";
import Modal from "../Modal";

export default class Card extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFavorite: false,
            isModalActive: false,
        }
    }
    componentDidMount = () => {
        const { favorites, barcode } = this.props;
          favorites.forEach(el => {
            if(el.barcode === barcode) {
                return this.setState({isFavorite: true})
            }
          })
    }
    showModal = () => {this.setState({isModalActive: true})}
    closeModal = () => {this.setState({isModalActive: false})}
    render(){
        const { name, price, picture, barcode, color, addFavorite, delFavorite, addToBasket } = this.props;
        return (
            <>
            <div className="img-wrapper">
                <img src={picture} alt={`${name} img`}/>
            </div>
            <div className="product-wrapper">
                <p className="product-name">{name}</p>
                <p className="product-barcode">product code: {barcode}</p>
                <p className="product-price">Price: {price}</p>
                <p className="product-color">Color: {color}</p>
                <div className="fav-icon__wrapper">
                {this.state.isFavorite === true ? 
                    <FontAwesomeIcon icon={favIcon} className="fav-icon" onClick={delFavorite} data-id={barcode}/> : 
                    <FontAwesomeIcon icon={noFav} className="fav-icon" onClick={addFavorite} data-id={barcode}/>}
                    <Button
                buttonId={barcode}
                text="Buy"
                click={this.showModal}
                />
                </div>
            </div>
            <Modal
            isActive= {this.state.isModalActive}
            modalClose= {this.closeModal}
            title="Do you want to add this product to basket?"
            okButton={addToBasket}
            cancelButton={this.closeModal}
            product={name}
            code={barcode}
            price={price}
            picture={picture}
            />
            </>
        )
    }
}

Card.propTypes = {
    picture: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.string,
    barcode: PropTypes.string,
    color: PropTypes.string,
    addFavorite: PropTypes.func,
    delFavorite: PropTypes.func,
    showModal: PropTypes.func,
    closeModal: PropTypes.func,
    addToBasket: PropTypes.func,
}

Card.defaultProps = {
    picture: "https://legacyogden.com/wp-content/uploads/2015/07/No-Image-Available1.png",
    name: "product name",
    price: "product price",
    barcode: "product code",
    color: "product color",
    addFavorite: ()=> {console.log("addFavorite");},
    delFavorite: ()=> {console.log('del fav');},
    showModal: ()=> {console.log("show modal");},
    closeModal: ()=> {console.log("close modal");},
    addToBasket: ()=> {console.log("to basket");},
}