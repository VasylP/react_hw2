import { Component } from "react";
import  PropTypes  from "prop-types";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import Button from '../Button'
import "./modal.scss";

export default class Modal extends Component {
    constructor (props) {
        super (props);
    }
    render (){
        const { isActive, title, modalClose, closeButton, okButton, cancelButton, product, price, code, text, picture } = this.props
        return isActive ? (
            <>
            <div className="modal-overlay"
                onClick={modalClose}
            >
                <div className="modal-window">
                    <div className="modal-header">
                        <h3>
                            {title}
                        </h3>
                        <div className="close-btn">
                            {closeButton === true ? <FontAwesomeIcon 
                            icon={faXmark} 
                            onClick={modalClose} 
                            /> : '' }
                        </div>
                    </div>
                    <div className="modal-text">
                        <img className="product-img" src={picture}/>
                        <div className="modal-text__product">
                        <p className="product-name">modal: {product}</p>
                        <p className="product-price">price: {price}</p>
                        <p className="product-code">code: {code}</p>
                        </div>
                    </div>
                    <p className="modal-text__text">{text}</p>
                    <div className="modal-btn">
                        <Button
                        buttonId={code}
                        text= "Ok"
                        click={okButton}
                        />
                        <Button
                        buttonId={code}
                        text= "Cancel"
                        click={cancelButton}
                        />
                    </div>
                </div>
            </div>
            </>
        ) : null;
    }
}

Modal.propTypes = {
    isActive: PropTypes.bool,
    modalClose: PropTypes.func,
    title: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    okButton: PropTypes.func,
    cancelButton: PropTypes.func,
    product: PropTypes.string,
    code: PropTypes.string,
    price: PropTypes.string,
  };
  Modal.defaultProps = {
    isActive: false,
    modalClose: () => {console.log("modal close");},
    title: "You want add this product?",
    closeButton: false,
    text: "If You click Ok you add this product to basket, if not, click Cancel",
    okButton: () => {console.log("confirmed");},
    cancelButton: () => {console.log("cancelled");},
    product: "product name",
    code: "product code",
    price: "product price",
  };