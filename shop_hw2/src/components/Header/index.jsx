import { Component } from "react";
import  PropTypes  from "prop-types";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { faBasketShopping } from "@fortawesome/free-solid-svg-icons";
import "./header.scss";

export default class Header extends Component {
    constructor(props){
        super(props);
    }
    render(){
        const { favCounter, basketCounter, favClick, basketClick } = this.props;
        return(
            <>
            <div className="header-wrapper">
                <div className="header__fav">
                    <span className="header-fav__counter">{favCounter}</span>
                    <FontAwesomeIcon icon={faStar} onClick={favClick}/>
                </div>
                <div className="header__bsk">
                    <span className="header-bsk__counter">{basketCounter}</span>
                    <FontAwesomeIcon icon={faBasketShopping} onClick={basketClick}/>
                </div>
            </div>
            </>
        )
    }
}

Header.propTypes = {
    favCounter: PropTypes.number,
    basketCounter: PropTypes.number,
    favClick: PropTypes.func,
    basketClick: PropTypes.func,
}
Header.defaultProps = {
    favCounter: null,
    basketCounter: null,
    favClick: () => {console.log("open fav page");},
    basketClick: () => {console.log("open basket page");},
}