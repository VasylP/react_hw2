import { Component } from "react";
import  PropTypes  from "prop-types";
import Card from "../Card";
import "./card-list.scss";

export default class CardList extends Component {
    constructor(props){
        super(props);
    }
    render(){
        const { products, favorites, addToFav, delFav, addToBasket } = this.props
        return (
            <>
            <ul className="card-list">
                {products.map(({ name, price, picture, barcode, color }) => (
                    <li key={barcode} className="card-list__item">
                        <Card
                        picture={picture}
                        name={name}
                        price={price}
                        barcode={barcode}
                        color={color}
                        addFavorite={addToFav}
                        delFavorite={delFav}
                        addToBasket={addToBasket}
                        favorites={favorites}
                        products={products}
                        />
                    </li>
                ))}
            </ul>
            </>
        )
    }
}

CardList.propTypes = {
    products: PropTypes.array,
    favorites: PropTypes.array,
    addToFav: PropTypes.func,
    addToBasket: PropTypes.func,
}
CardList.defaultTypes = {
    products: [],
    favorites: [],
    addToFav: () => {console.log("add to Fav");},
    addToBasket: () => {console.log("add to Basket");},
}